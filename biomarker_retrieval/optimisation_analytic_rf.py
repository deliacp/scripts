#!/usr/bin/env python
"""
Analytic maximum of RF

@author: Delia Casas Pastor delia_93_carba@hotmail.com
"""
##Add directory of Classifiers_module
import os
import sys
sys.path.append(os.getcwd())
from Classifiers_module.classifiers import RForest
import rpy2.robjects as ro
import numpy as np
import matplotlib.pyplot as plt
import csv


def score_classif(n_est=10, iterat=200,
                  arg1="Results/polices_result/Experimentunion_model.txt",
                  arg2="Results/RNAseq_and_tiling_preprocessed"):
    """
    Scores the results of the classifier. It defines the energy function.

    Arguments:
        n_est: number of trees per forest. Default is 10
            TO OPTIMISE
        iterat: number of iterations of the classifier. Default is 200
        arg1: path to the results of RGIFE.
        arg2: path to the preprocessed gene expression table
    """
    # Run classifier
    RForest(n_est=n_est, iterat=iterat, arg1=arg1, arg2=arg2)
    # Score the retireved biomarkers
    r_code = ro.r.source("Scoring_classifier_py.R")
    path_RF = "Results/RandomForest_output.txt"
    sol = ro.r.scoring(path_RF)
    return(float(sol[0]))


def evaluation(n_est=10, n_rep=3):
    """Evaluates the objective function n_rep times with a n_est=n_est"""
    ev = []
    for a in xrange(0, n_rep):
        ev.append(score_classif(n_est=n_est))
    return(ev)

# Evaluate 3 times per value of the n_est defining a dictionary
rep = 3
result = {}
search_space = np.arange(5, 51, 1)
for a in search_space:
    result[a] = []
    result[a] = evaluation(n_est=a, n_rep=rep)

# Sort identifiers of dictionary
k_sort = sorted(result.keys())

# Save the results
p_res = "Results/analytic_rf_res.csv"
with open(p_res, "w") as f:
    write = csv.writer(f, delimiter="\t")
    write.writerow(k_sort)
    for a in xrange(rep):
        row = []
        for z in k_sort:
            row.append(result[z][a])
        write.writerow(row)

# Box plot
fig, ax = plt.subplots(figsize=(25, 7))

# Boxplot settings
box = plt.boxplot([result[a] for a in k_sort], labels=k_sort)
plt.setp(box["boxes"], color="black", linewidth=2)
plt.setp(box["whiskers"], color="black", linewidth=2)
plt.setp(box["fliers"], color="black", linewidth=2)
plt.setp(box["caps"], color="black", linewidth=2)
plt.setp(box["medians"], color="brown", linewidth=3)

# Axes labels
ax.set_title("GO score vs number of trees in the forest")
ax.set_xlabel("Number of trees (T)")
ax.set_ylabel("GO score")
font = {'family' : 'normal',
        'weight' : 'bold',
        'size'   : 17}
plt.rc("font", **font)

# Axes ranges
all_rec = []
for key in result:
    for element in result[key]:
        all_rec.append(element)

ax.set_ylim(min(all_rec)-0.05, max(all_rec)+0.05)
plt.locator_params(axis="x")
plt.locator_params(axis="y")

# save
plt.savefig("Results/analytic_RF.png", dpi=600)
