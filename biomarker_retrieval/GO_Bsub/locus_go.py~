#!/usr/bin/env python
"""
Create a dictionary to match GO and locus_tag for all the entries in uniprot.

    Input: B. subtilis 168 Uniprot flat file
    Output: "coversion_locus_go.txt" tab separated file
        Column 1: Locus tag
        Column 2: GO terms separated by single spaces

Created on Thu Jun 23 12:03:45 2016

@author: Delia Casas Pastor delia_93_carba@hotmail.com
"""


#from Bio import SeqIO
import re
import csv


#Initialise variables
go_map = {}
part = []
go_pat = re.compile("GO;", re.IGNORECASE)
locus_pat = re.compile("OrderedLocusNames=")
end_pat = re.compile("//")
dr_pat = re.compile("DR")
gn_pat = re.compile("GN")

#Iterate over the uniprot.txt file to retrive locus and go
go = []
with open("uniprot.txt", "rU") as f:
    for line in f:
        if (re.match(gn_pat, line) is not None):  # locus in GN tag
            line_l = line.split(" ")
            for sub_line in line_l:
                if (re.match(locus_pat, sub_line) is not None):
                    locus = sub_line.split("=")[1].split(";")[0]
        if (re.match(dr_pat, line) is not None):  # GO term in DR tag
            line_l = line.split(" ")
            if (re.match(go_pat, line_l[3]) is not None):
                go.append(line_l[4].split(";")[0])
        if (re.match(end_pat, line) is not None):
            if (len(locus) != 0):
                go_map[locus] = go
            locus = []
            go = []

#Generate output file
with open('conversion_locus_go.txt', 'wb') as f:
    write = csv.writer(f, delimiter='\t')
    for a in go_map:
        write.writerow([a, " ".join(go_map[a])])
