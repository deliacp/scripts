#!/usr/bin/env python
"""
Create a dictionary to match GO and locus_tag for all the entries in uniprot.

    Input: B. subtilis 168 Uniprot flat file
    Output: "coversion_locus_len.txt" tab separated file
        Column 1: Locus tag
        Column 2: length of the gene (len protein * 3)

Created on Tue Jul 26 2016

@author: Delia Casas Pastor delia_93_carba@hotmail.com
"""


import re
import csv


#Initialise variables
len_map = {}
go_pat = re.compile("GO;", re.IGNORECASE)
locus_pat = re.compile("OrderedLocusNames=")
end_pat = re.compile("//")
gn_pat = re.compile("GN")
id_pat = re.compile("ID")

#Iterate over the uniprot.txt file to retrive locus and lenth
length = []
with open("uniprot.txt", "rU") as f:
    for line in f:
        if (re.match(gn_pat, line) is not None):  # locus in GN tag
            line_l = line.split(" ")
            for sub_line in line_l:
                if (re.match(locus_pat, sub_line) is not None):
                    locus = sub_line.split("=")[1].split(";")[0]
        if (re.match(id_pat, line) is not None):  # lenth in ID tag
            line_id = line.split(";")
            line_id = line_id[1].replace(" ", "")
            length = (int(line_id[:-4])*3)  # Multiplying by 3
        if (re.match(end_pat, line) is not None):
            if (len(locus) != 0):
                len_map[locus] = length
            locus = []
            length = []

#Generate output file
with open('conversion_locus_len.txt', 'wb') as f:
    write = csv.writer(f, delimiter='\t')
    for a in len_map:
        write.writerow([a, len_map[a]])
