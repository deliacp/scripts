#!/usr/bin/env python

def RForest(arg1, arg2, n_est=10, iterat=200):
    """
    Random forest classifier to reduce RGIFE result to 1 feature
    
    This algorithm is prepared to run from command line using the format:
    
    python RForest.py <polices result> <Expression matrix> <number of features
    considered per bifurcation> <number of iterations of the classifier>
    
    The expression matrix has to contain the the experiment ID as first row and
    the gene identifiers as first column. Last row would be the assigment of the
    sample to STRESS or CONTROL.
    
    Polices result would be the file Experimentunion_model.txt produced as output
    of polices.py as part of RGIFE algorithm
    
    Created on Mon Jun 27 2016
    
    @author: Delia Casas Pastor
    """
    from sklearn.ensemble import RandomForestClassifier
    import csv
    from collections import defaultdict
    import os
    import sys


    def read_tidy(input_matrix):
        """
        Read Normalised gene expression matrix and removes uniformative cols
        """
        #Read the Normalised Matrix. Source http://stackoverflow.com/questions/16503560/read-specific-columns-from-csv-file-with-python-csv
        matrix = defaultdict(list)
        with open(input_matrix, 'rb') as f:
            r = csv.DictReader(f, delimiter='\t')
            for row in r:
                for (k, v) in row.items():
                    matrix[k].append(v)
        #Remove uniformative columns
        if [a == 'StartV3' for a in matrix]:
            cols_to_remove = ['StartV3', 'EndV3', 'StartV2', 'EndV2', 'Strand',
                              'Keeptot', 'Id260210', 'OrigId', 'classif',
                              'Name', 'Keeptrim']
            new = remove_col(matrix, cols_to_remove)
        else:
            new = matrix
        return(new)

    def remove_col(dictionary, id_col):
        """
        Remove entries for uninformative columns
        """
        for a in id_col:
            del dictionary[a]
        return(dictionary)

    def select_row(identifiers, dictionary):
        """
        Keeps only data corresponding to the genes whose locus is contained in
        identifiers list
        """
        ind = []
        dictionary_trim = {}
        for a in identifiers:
            ind.append(dictionary['Locus'].index(a))
        for a in dictionary:
            dictionary_trim[a] = []
            for z in ind:
                dictionary_trim[a].append(dictionary[a][z])
            dictionary_trim[a].append(dictionary[a][len(dictionary[a])-1])
        return(dictionary_trim)

    def Classifier(matrix, id, rep=1):
        if rep < len(id)+rep-1:
            #Select rows with the features contained in Experimentunion_model.txt
            matrix_trim = select_row(id, matrix)
            Locus = matrix_trim['Locus'][:-1]
            #List of the classes per sample (0: control; 1: stress)
            attributes = {'CONTROL': 0, 'STRESS': 1}
            Y = []
            for a in matrix_trim:
                if a != 'Locus':
                    Y.append(attributes[matrix_trim[a][-1]])
            #Each position of the list is a experiment (269) which contains the exp
                #data for all the genes (21 in this case)
            X = []
            for a in matrix_trim:
                if a != 'Locus':
                    Xi = []
                    for z in xrange(len(matrix_trim[a])-1):
                        Xi.append(float(matrix_trim[a][z]))
                    X.append(Xi)
            #Run randomforestclassifier
            clf = RandomForestClassifier(n_estimators=n_est)  # Try different
            clf = clf.fit(X, Y)
            #Assign importance to feature
            Importances = clf.feature_importances_
            Importance_summary = {}
            for i, a in enumerate(Locus):
                Importance_summary[a] = []
                Importance_summary[a] = Importances[i]
            #Remove the less important feature
            Importance_sorted = sorted(Importance_summary,
                                       key=Importance_summary.get, reverse=True)
            new_matrix_trim = select_row(Importance_sorted[:-1], matrix_trim)
            m, i = Classifier(new_matrix_trim, Importance_sorted[:-1], rep+1)
            return(m, i)
        else:
            return(matrix, id)

    def run_iter(n, matrix, id):
        """
        Run several times the classifier and get the proprotion of best biomarker
        """
        set_locus = []
        print('Starting classifier')
        f = 1
        for a in xrange(n):
            new_matrix, locus = Classifier(matrix, id)
            set_locus.append(locus[0])
            #print('{0:.1f}% completed'.format(f/float(n)*100))
            f += 1
        freq_raw = {a: (set_locus.count(a)/float(n)*100) for a in set_locus}
        freq_s = sorted(freq_raw, key=freq_raw.get, reverse=True)
        return(freq_raw, freq_s)
    
    print(("\nThe number of iterations is {}").format(iterat))
    print(("The number of trees per iteration is {}".format(n_est)))
    
    #Read the identifiers file (union of rgife)
    ids = []
    with open(arg1, 'r') as f:
        for line in f:
            line = line.strip()
            ids.append(line.strip('"'))

    #Read Normalised Matrix
    matrix_in = read_tidy(str(arg2))

    #Run several times the classifier and get the proportion of best biomarker
    freq, freq_sorted = run_iter(int(iterat), matrix_in, ids)

    print('{0} is the highest scoring biomarker in {1:.2f}% of the' \
    ' iterations'.format(freq_sorted[0], freq[freq_sorted[0]]))

    #Generate output file
    with open('Results/RandomForest_output.txt', 'wb') as f:
        write = csv.writer(f, delimiter='\t')
        for a in freq_sorted:
            write.writerow([a, freq[a]])
    print('Classification finished')



def SVM(arg1, arg2, penalty=1, iterat=200):
    """
    Suppor Vector Machine classifier to reduce RGIFE result to 1 feature
    
    This algorithm is prepared to run from command line using the format:
    
    python SVM.py <polices result> <Expression matrix>
    
    The expression matrix has to contain the the experiment ID as first row and
    the gene identifiers as first column. Last row would be the assigment of the
    sample to STRESS or CONTROL sets.
    
    Polices result would be the file Experimentunion_model.txt produced as output
    of polices.py as part of RGIFE algorithm.
    
    Created on Wed Jun 8 17:30:00 2016
    
    @author: Delia Casas Pastor
    """
    from sklearn.svm import LinearSVC
    import csv
    from collections import defaultdict
    import sys
    from sklearn.feature_selection import RFE
    import os

    def read_tidy(input_matrix):
        """
        Read Normalised gene expression matrix and remove uniformative cols
        """
        #Read the Normalised Matrix From http://stackoverflow.com/questions/16503560/read-specific-columns-from-csv-file-with-python-csv
        matrix = defaultdict(list)
        with open(input_matrix, 'rb') as f:
            r = csv.DictReader(f, delimiter='\t')
            for row in r:
                for (k, v) in row.items():
                    matrix[k].append(v)
        #Remove uniformative columns
        if [a == 'StartV3' for a in matrix]:
            cols_to_remove = ['StartV3', 'EndV3', 'StartV2', 'EndV2', 'Strand',
                              'Keeptot', 'Id260210', 'OrigId', 'classif',
                              'Name', 'Keeptrim']
            new = remove_col(matrix, cols_to_remove)
        else:
            new = matrix
        return(new)

    def remove_col(dictionary, id_col):
        """
        Remove entries for selected columns
        """
        for a in id_col:
            del dictionary[a]
        return(dictionary)

    def select_row(identifiers, dictionary):
        """
        Keep data corresponding to only the genes whose locus is contained in
        identifiers list
        """
        ind = []
        dictionary_trim = {}
        for a in identifiers:
            ind.append(dictionary['Locus'].index(a))
        for a in dictionary:
            dictionary_trim[a] = []
            for z in ind:
                dictionary_trim[a].append(dictionary[a][z])
            dictionary_trim[a].append(dictionary[a][len(dictionary[a])-1])
        return(dictionary_trim)

    def Classifier(matrix, id, rep=1):
        """
        Run the classifier
        Args:
            id: locus tag of the genes contained in Experimentunion_model.txt
            matrix: expression matrix as returned by read_tidy()
            rep: internal counter to stop the recursivity of the function
        """
        #Select rows with the features contained in Experimentunion_model.txt
        matrix_trim = select_row(id, matrix)
        Locus = matrix_trim['Locus'][:-1]
        #List of the classes per sample (0: control; 1: stress)
        attributes = {'CONTROL': 0, 'STRESS': 1}
        Y = []
        for a in matrix_trim:
            if a != 'Locus':
                Y.append(attributes[matrix_trim[a][-1]])
        #Each position of the list is an experiment (269) that contains the exp
            #data for all the genes (number depends on RGIFE)
        X = []
        for a in matrix_trim:
            if a != 'Locus':
                Xi = []
                for z in xrange(len(matrix_trim[a])-1):
                    Xi.append(float(matrix_trim[a][z]))
                X.append(Xi)
        #Run svm classifier
        estimator = LinearSVC(C=float(penalty))
        sel = RFE(estimator, n_features_to_select=1, step=1)
        sel = sel.fit(X, Y)
        #Extract the highest scoring feature
        rank = sel.ranking_
        classif = []
        for a in xrange(len(rank)):
            if rank[a] == 1:
                classif.append(Locus[a])
        return(classif)

    def run_iter(n, matrix, id):
        """
        Run several times the classifier and get the proprotion of best biomarker
        """
        set_locus = []
        print('Starting classifier')
        f = 1
        for a in xrange(n):
            locus = Classifier(matrix, id)
            set_locus.append(locus)
            #print('{0:.1f}% completed'.format(f/float(n)*100))
            f += 1
        set_locus_list = []
        for a in set_locus:
            set_locus_list.append(a[0])
        freq_raw = {a: (set_locus_list.count(a)/float(n)*100)
        for a in set_locus_list}
        freq_s = sorted(freq_raw, key=freq_raw.get, reverse=True)
        return(freq_raw, freq_s)

    # Read the identifiers file (union of rgife)
    print("\nThe number of iterations is {}".format(iterat))
    print("The penalty for the error term is {}".format(penalty))
    ids = []
    with open(str(arg1), 'r') as f:
        for line in f:
            line = line.strip()
            ids.append(line.strip('"'))

    # Read Normalised Matrix
    matrix_in = read_tidy(str(arg2))

    # Run several times the classifier and get the proprotion of best biomarker
    freq, freq_sorted = run_iter(iterat, matrix_in, ids)

    #print('===============================================')
    print('{0} is the highest scoring biomarker in {1:.2f}% of the' \
    ' iterations'.format(freq_sorted[0], freq[freq_sorted[0]]))
    
    #Generate output file
    with open('Results/SVM-RFE_output.txt', 'wb') as f:
        write = csv.writer(f, delimiter='\t')
        for a in freq_sorted:
            write.writerow([a, freq[a]])
    #print('=================================================')
    print('Classifier finished')


if __name__ == "__main__":
    """ Run classifier from command line.
    
    python classifiers.py <random_forest|svm> <parameter> <directory where 
    /Results/polices_result/ and /Results/RNAseq_and_tiling_preprocessed are (optional, default = working directory)>

    where parameter is the number of trees in the forest for random_forest
    (default = 10) or the penaly of the error term in svm (default = 1.0).
    
    It takes the results of RGIFE union and the expression table contained in
    RNAseq_and_tiling_preprocessed.
    
    Value:
        The results are saved in /Results/RandomForest_output.txt or in
        /Results/SVM-RFE_output.txt
    """
    import sys
    import os

    argum = sys.argv
    try:
        os.chdir(argum[3])
    except IndexError:
	argum.append(os.getcwd())

    if argum[1] == "random_forest":
        try:
            argum[2]
        except IndexError:
            print("\nThe default number of trees (10) to be applied")
            n_t = 10
        else:
            n_t = int(argum[2])
        s = "/"
        RForest("Results/polices_result/Experimentunion_model.txt",
                "Results/RNAseq_and_tiling_preprocessed",
                n_est=n_t, iterat=200)
    elif argum[1] == "svm":
        try:
            argum[2]
        except IndexError:
            print("\nThe default penalty for error term (1.0) to be applied")
            n_e = 1.0
        else:
            n_e = float(argum[2])
        SVM("Results/polices_result/Experimentunion_model.txt",
            "Results/RNAseq_and_tiling_preprocessed",
            penalty=n_e, iterat=200)
    else:
        print("The first argument must be either 'random_forest' or 'svm'")
    
