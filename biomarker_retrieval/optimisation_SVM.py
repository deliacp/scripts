#!/usr/bin/env python
"""
optimistion_SVM is a heuristic to optimise the number of trees in the forest
that uses simulated annealing (source of the base metodh is 
https://github.com/perrygeo/simanneal).
The annealing max and min temperatures and the number of stpes are defined in
the variables "tmax", "tmin" and "steps".
".updates" defines the total number of times the state are plotted.

Requirements:
- Download the simulated annealing algorithm included in 
    https://github.com/perrygeo/simanneal and add the path to sys.path variable
- Folders:
    "~/Results/polices_result/Experimentunion_model.txt",
    "~/Results/RNAseq_and_tiling_preprocessed":
    Containing the results of RGIFE  and the gene
    expression intensity after the preprocessing, respectively.
- Classifiers_module.classifiers.SVM function
- R script Scoring_classifier_py.R

Change the paths to all these files in the script if executed from IDE or
run from Linux command line from the folder where those files are contained
using:
    python optimisation_SVM.py

The parameter to be optimised is the penalty of the error term, whose searching
space is set between 0.1 and 4.6 in steps of 0.1 but it could be changed in the
source script (line 107 aprox, variable search_space).

@author: delia casas pastor delia_93_carba@hotmail.com
"""
#Add directory of classifiers and simulated annealing to path to run them as modules
import sys
sys.path.append("/home/delia/Downloads/Trials/biomarker")
sys.path.append("/home/delia/Downloads/simanneal-master")

try:
    from simanneal2 import Annealer  # Folder where simulated annealing is saved (from https://github.com/perrygeo/simanneal)
except ImportError:
    print("Change the path to the simanneal module")
    sys.exit()

import random

try:
    from Classifiers_module.classifiers import SVM
except ImportError:
    print("Change the path to the Classifiers_module")
    sys.exit()

import os
import rpy2.robjects as ro


# Based on salesman example in https://github.com/perrygeo/simanneal
# package taken from there
def score_classif(penalty=1.0, iterat=200,
                  arg1="Results/polices_result/Experimentunion_model.txt",
                  arg2="Results/RNAseq_and_tiling_preprocessed"):
    """
    Scores the results of the classifier. It defines the energy function.

    Arguments:
        n_est: number of trees per forest. Default is 10
            TO OPTIMISE
        iterat: number of iterations of the classifier. Default is 200
        arg1: path to the results of RGIFE.
        arg2: path to the preprocessed gene expression table
    """
    # Run classifier
    SVM(penalty=penalty, iterat=iterat, arg1=arg1, arg2=arg2)
    # Score the retireved biomarkers
    r_code = ro.r.source("Scoring_classifier_py.R")
    path_RF = "Results/RandomForest_output.txt"
    sol = ro.r.scoring(path_RF)
    return(float(-sol[0]))

class TreeOptimisation(Annealer):
    """
    Test annealer with a SVM optimisation problem.
    """
    def __init__(self, state, range_values):
        super(TreeOptimisation, self).__init__(state)

    def move(self):
        """Changes the parameter value.
            The number of possible neighbours is 10 except for states at the
            ends of the range of values in the searching space.
        """
        if self.state >= (max(search_space) - 90):
            new = random.randrange(self.state - 100, max(search_space), 10)
            while new == self.state:
                new = random.randrange(self.state - 100, max(search_space), 10)
        elif self.state <= (min(search_space) + 90):
            new = random.randrange(min(search_space), self.state + 100, 10)
            while new == self.state:
                new = random.randrange(min(search_space), self.state + 100, 10)
        else:
            new = random.randrange(self.state - 100, self.state + 100, 10)
            while new == self.state:
                new = random.randrange(self.state - 100, self.state + 100, 10)
        self.state = new

    def energy(self):
        """Calculates the score of the parameter defined in state."""
        return(score_classif(penalty=float(self.state)/100, iterat=200))

    def save_state(self, fname="Results/Simulated_annealing_result_SVM"):
        """Saves the optimum state"""
        print("Saving state to: %s" % fname)
        with open(fname, "w") as fh:
            fh.write(str(self.state))

# Range of values for the number of trees per classifier
search_space = range(10, 470, 10)
# initial state of the number of trees
init_state = random.randrange(min(search_space), max(search_space), 10)

# Load an instance
tsp = TreeOptimisation(init_state, search_space)
# copy the initial state
tsp.copy_strategy = "deepcopy"
# Set schedule
schedule = {
    "tmax": 0.5,
    "tmin": 0.00001,
    "steps": 30
}
tsp.set_schedule(schedule)
tsp.updates = 30
# Run simulated annealing
try:
    state, e = tsp.anneal()
except IOError:
    print("Change the path to Experimentunion_model.txt and RNAseq_results_preprocessed")
    sys.exit()

# i is the GO score of the optimum solution and state the optimun #trees
print("The highest GO score was {} with an error term penalty of {}.".format(-e, float(state)/100))
  
