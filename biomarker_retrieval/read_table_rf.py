#!/usr/bin/env python
"""
Read the table of results of analytic optimisation and plot again
Created on Fri Jul  8 09:37:05 2016

@author: delia delia_93_carba@hotmail.com
"""
import csv
import numpy as np
import matplotlib.pyplot as plt

vec = []
with open("Results/analytic_rf_res.csv") as f:
    reader = csv.reader(f, delimiter="\t")
    i = 1
    for row in reader:
        if i == 1:
            for el in row:
                vec.append([int(el)])
                i += 1
        else:
            for num, el in enumerate(row):
                vec[num].append(float(el))
result = {}
for a in vec:
    result[a[0]] = a[1:]

# Plot
#x = []
#y = []
#mean = []
#for a in result.keys():
#    x.append(np.repeat(a, len(result[a])))
#    y.append(result[a])
#    mean.append(np.mean(result[a]))

# Sort identifiers of dictionary
k_sort = sorted(result.keys())

# Box plot
fig, ax = plt.subplots(figsize=(25, 7))

# Boxplot settings
box = plt.boxplot([result[a] for a in k_sort], labels=k_sort)
plt.setp(box["boxes"], color="black", linewidth=2)
plt.setp(box["whiskers"], color="black", linewidth=2)
plt.setp(box["fliers"], color="black", linewidth=2)
plt.setp(box["caps"], color="black", linewidth=2)
plt.setp(box["medians"], color="brown", linewidth=3)

# Axes labels
ax.set_title("GO score vs number of trees in the forest")
ax.set_xlabel("Number of trees (T)")
ax.set_ylabel("GO score")
font = {'family' : 'normal',
        'weight' : 'bold',
        'size'   : 17}
plt.rc("font", **font)

# Axes ranges
all_rec = []
for key in result:
    for element in result[key]:
        all_rec.append(element)

ax.set_ylim(min(all_rec)-0.05, max(all_rec)+0.05)
plt.locator_params(axis="x")
plt.locator_params(axis="y")

# save
plt.savefig("Results/analytic_RF.png", dpi=600)
